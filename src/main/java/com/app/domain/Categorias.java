package com.app.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "categoria", schema = "concursapy")
public class Categorias implements Serializable {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@SequenceGenerator(name = "CATEGORIA_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "concursapy.categoria_seq")
	@GeneratedValue(generator = "CATEGORIA_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_categoria")
	private Long idCategoria;
	
 	@Column(name = "descripcion")
	private String descripcion;

	public Long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Long idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


}
