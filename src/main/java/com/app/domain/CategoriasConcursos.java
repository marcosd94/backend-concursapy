package com.app.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "categorias_concurso", schema = "concursapy")
public class CategoriasConcursos  implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@SequenceGenerator(name = "CATEGORIAS_CONCURSOS_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "concursapy.categorias_concursos_seq")
	@GeneratedValue(generator = "CATEGORIAS_CONCURSOS_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_categorias_concursos")
	private Long idCategoriasConcursos;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_concurso", nullable = false)
	@NotNull
	private Concursos idConcurso;
 	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_categoria", nullable = false)
	@NotNull
	private Categorias idCategoria;

	public Long getIdCategoriasConcursos() {
		return idCategoriasConcursos;
	}

	public void setIdCategoriasConcursos(Long idCategoriasConcursos) {
		this.idCategoriasConcursos = idCategoriasConcursos;
	}

	public Concursos getIdConcurso() {
		return idConcurso;
	}

	public void setIdConcurso(Concursos idConcurso) {
		this.idConcurso = idConcurso;
	}

	public Categorias getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Categorias idCategoria) {
		this.idCategoria = idCategoria;
	}
	

}
