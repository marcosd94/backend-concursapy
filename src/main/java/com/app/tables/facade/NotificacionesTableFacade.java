package com.app.tables.facade;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.naming.NamingException;

import com.app.domain.Notificaciones;
import com.app.tables.dao.NotificacionesTableDAO;
import com.app.utils.NotificacionesRespuesta;
import com.app.utils.tables.facade.DatatablesFacadeImpl;

import us.raudi.pushraven.Notification;
import us.raudi.pushraven.Pushraven;


public class NotificacionesTableFacade extends
        DatatablesFacadeImpl<Notificaciones> {

    @Inject
    NotificacionesTableDAO notificacionesServiciosDao;

    @PostConstruct
    public void init() {
        if (this.dao == null) {
            this.dao = notificacionesServiciosDao;
        }
    }
    public List<Map<String, Object>> getNotificaciones(String idFirebase) {
        return notificacionesServiciosDao.getNotificaciones(idFirebase);
    }

    public String crearNotificaciones(Notificaciones noti) throws SQLException, NamingException {
    	return notificacionesServiciosDao.crearNotificaciones(noti);
        
    }
    
    public Notificaciones eliminarNotificaciones(Long id) throws SQLException, NamingException {
    	return notificacionesServiciosDao.eliminarNotificaciones(id);
        
    }
    
    public String modificarNotificacion(Notificaciones notificaciones) throws SQLException, NamingException {
    	return notificacionesServiciosDao.modificarNotificacion(notificaciones);
        
    }
    
    public Notificaciones marcarNotificacionLeida(Long idNotificacion) throws SQLException, NamingException {
    	return notificacionesServiciosDao.marcarNotificacionLeida(idNotificacion);
        
    }
    
    public NotificacionesRespuesta enviarNotificaciones() throws SQLException, NamingException {
    	NotificacionesRespuesta respuesta = new NotificacionesRespuesta();
    	final String key = "AAAA-6GNVKs:APA91bG4wGoqRGN0Q_wZOOKcbmL7ced5C3Osbx9Q0uZpW-_UT0Wv65J2yH4dmD_nZzmhVDVCU1k5BagZSBmGiPHzU1Kx8Ycpm3mXOIUMSezUOFMzHUqcpN87f0af3HvngrL26MV-N_K6";
    	Pushraven.setKey(key);
    	Notification raven = new Notification();
    	List<Map<String, Object>> listTokens = notificacionesServiciosDao.getListaNotificaciones();
    	String estado = "No se cuenta con notificaciones a enviar";
    	if(listTokens.size() > 0){
    		for(Map<String,Object> tokens : listTokens){
    			String token = tokens.get("token").toString();
            	raven.title("ConcursaPy")
            	  .text("Tiene nuevos concursos en postulación")
            	  .color("#ff0000")
            	  .to(tokens.get("token").toString());
            	raven.addRequestAttribute("delay_while_idle", true); // for request attributes
            	raven.addNotificationAttribute("color", "#ff0000"); // for notification attirubtes

        		Pushraven.setNotification(raven); // if not already set
        		Pushraven.push();
        		raven.clear(); // clears the notification, equatable with "raven = new Notification();"
        		raven.clearAttributes(); // clears FCM protocol paramters excluding targets
        		raven.clearTargets(); // only clears targets
    		}
    		notificacionesServiciosDao.marcarNotificacionesEnviadas();
    		estado= "Notificaciones enviadas correctamente";
    	}
    	respuesta.setCantidad(listTokens.size());
    	respuesta.setEstado(estado);
		return respuesta;
        
    }
}
