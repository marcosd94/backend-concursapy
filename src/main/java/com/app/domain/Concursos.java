package com.app.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "concursos", schema = "concursapy")
public class Concursos implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@SequenceGenerator(name = "CONCURSOS_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "concursapy.concursos_seq")
	@GeneratedValue(generator = "CONCURSOS_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_concursos")
	private Long idConcursos;
	
 	@Column(name = "descripcion_oee")
	private String descripcionOee;
 	
	@Column(name = "cargo")
	private String cargo;
	
	@Column(name = "puesto")
	private String puesto;
	
	@Column(name = "tipos_concurso")
	private String tiposConcurso;
	
	@Column(name = "vacancia")
	private Integer vacancia;
	
	@Column(name = "inicio_postulacion")
	private Date inicioPostulacion;
	
	@Column(name = "fin_postulacion")
	private Date finPostulacion;
	
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;

	public Long getIdConcursos() {
		return idConcursos;
	}

	public void setIdConcursos(Long idConcursos) {
		this.idConcursos = idConcursos;
	}

	public String getDescripcionOee() {
		return descripcionOee;
	}

	public void setDescripcionOee(String descripcionOee) {
		this.descripcionOee = descripcionOee;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getTiposConcurso() {
		return tiposConcurso;
	}

	public void setTiposConcurso(String tiposConcurso) {
		this.tiposConcurso = tiposConcurso;
	}

	public Integer getVacancia() {
		return vacancia;
	}

	public void setVacancia(Integer vacancia) {
		this.vacancia = vacancia;
	}

	public Date getInicioPostulacion() {
		return inicioPostulacion;
	}

	public void setInicioPostulacion(Date inicioPostulacion) {
		this.inicioPostulacion = inicioPostulacion;
	}

	public Date getFinPostulacion() {
		return finPostulacion;
	}

	public void setFinPostulacion(Date finPostulacion) {
		this.finPostulacion = finPostulacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
