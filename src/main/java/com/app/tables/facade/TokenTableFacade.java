package com.app.tables.facade;

import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.app.domain.Token;
import com.app.tables.dao.TokenTableDAO;
import com.app.utils.tables.facade.DatatablesFacadeImpl;


public class TokenTableFacade extends
        DatatablesFacadeImpl<Token> {

    @Inject
    TokenTableDAO tokenServiciosDao;

    @PostConstruct
    public void init() {
        if (this.dao == null) {
            this.dao = tokenServiciosDao;
        }
    }
    
    public Token registrarToken(Long idUsuario, String token) throws SQLException{
    	return tokenServiciosDao.registrarToken(idUsuario, token);
        
    }
}
