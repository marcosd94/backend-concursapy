package com.app.service;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.app.domain.Token;
import com.app.tables.facade.TokenTableFacade;
import com.app.utils.ErrorMessage;

@Named
@RequestScoped
@Path("/rest/token")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TokenResource {

	@Inject
	TokenTableFacade facade;

	@POST
	@Path("/insertar-registration-token")
	@Transactional
	public Response registrarToken(@QueryParam("idUsuario") Long idUsuario, @QueryParam("token") String token)
			throws Exception {
		Token itoken;
		if (idUsuario == null || idUsuario.equals("")) {
			throw new Exception("Campo idUsuario es Obligatorio");
		}
		if (token == null || token.equals("")) {
			throw new Exception("Campo token es Obligatorio");
		}
		try {
			itoken = facade.registrarToken(idUsuario, token);
			return Response.ok(itoken).build();

		} catch (PersistenceException e) {
			ErrorMessage error = new ErrorMessage();
			error.setMessage("Ya existe el registration token para el usuario");
			error.setCause(e.getCause());
			return Response.ok(error).build();
		}

	}
}
