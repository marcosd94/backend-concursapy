package com.app.service;

import java.sql.SQLException;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.app.domain.Suscripciones;
import com.app.tables.facade.SuscripcionesTableFacade;

@Named
@RequestScoped
@Path("/rest/suscripciones")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SuscripcionesResource {

    @Inject
    SuscripcionesTableFacade facade;

    @GET
    @Path("/listar-suscripciones")
    public Response getSuscripciones(@QueryParam("idUsuario")  Long idUsuario, @QueryParam("descripcion") String descripcion) {
        return Response.ok(facade.getSuscripciones(idUsuario,descripcion)).build();
    }

    @POST
    @Path("/agregar")
    @Transactional
    public Response crearSuscripciones(@QueryParam("idUsuario")  Long idUsuario,
    		@QueryParam("idCategoria")  Long idCategoria)
            throws Exception {  	
        	if(idUsuario== null || idUsuario.equals("")){
        		throw new Exception("Campo idUsuario es Obligatorio");
        	}    	
        	if(idCategoria== null || idCategoria.equals("")){
        		throw new Exception("Campo idCategoria es Obligatorio");
        	}
        	Long suscripciones = facade.crearSuscripciones(idUsuario, idCategoria);
            return Response.ok(suscripciones).build();
    }
    @POST
    @Path("/inactivar-suscripcion")
    public Response inactivarSuscripciones(@QueryParam("idSuscripcion")  Long idSuscripcion) throws SQLException, NamingException{
			return Response.ok(facade.inactivarSuscripciones(idSuscripcion)).build();
    }

    @GET
    @Path("/graficos")
    public Response getTotalSuscripciones(@QueryParam("categoria")  String categoria, @QueryParam("descripcion") String descripcion, @QueryParam("anho") String anho) {
        return Response.ok(facade.getTotalSuscripciones(categoria,descripcion,anho)).build();
    }
}
