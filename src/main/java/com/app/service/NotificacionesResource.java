package com.app.service;

import java.net.URISyntaxException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.app.domain.Notificaciones;
import com.app.tables.facade.NotificacionesTableFacade;
import com.app.utils.NotificacionesRespuesta;

@Named
@RequestScoped
@Path("/rest/notificaciones")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class NotificacionesResource {

    @Inject
    NotificacionesTableFacade facade;

    @GET
    @Path("/listar-notificaciones")
    public Response getNotificaciones(@QueryParam("idFirebase")  String idFirebase) {
        return Response.ok(facade.getNotificaciones(idFirebase)).build();
    }
    
    @POST
    @Path("/agregar")
    @Transactional
    public String crearNotificaciones(@QueryParam("estado")  Boolean estado,
    		@QueryParam("fechaEnvio")  String fechaEnvio)
            throws URISyntaxException {
    	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
        	if(estado == null || estado.equals(""))
        		return "Campo estado es Obligatorio";
        	if(fechaEnvio == null || fechaEnvio.equals(""))
        		return "Campo fechaEnvio es Obligatorio";
    		Notificaciones noti = new Notificaciones();
    		noti.setEstadoNotificacion(estado);
    		noti.setFechaEnvio((Date) formatter.parse(fechaEnvio));
            return facade.crearNotificaciones(noti);
            
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR" + e.getMessage();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR" + e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Fecha Envio inv�lida";
		}
    }
    
    @POST
    @Path("/modificar-notificacion")
    public Response modificarNotificacion(Notificaciones notificaciones) throws SQLException, NamingException{
		return Response.ok(facade.modificarNotificacion(notificaciones)).build();
    }
        
    @POST
    @Path("/marcar-notificacion-leida")
    public Response marcarNotificacionLeida(@QueryParam("idNotificaciones")  Long idNotificaciones) throws Exception{
    	if(idNotificaciones== null || idNotificaciones.equals("")){
    		throw new Exception("Se requiere el identificador de la notificación");
    	}
		return Response.ok(facade.marcarNotificacionLeida(idNotificaciones)).build();
    }

    @POST
    @Path("/eliminar")
    public Response eliminarNotificaciones(@QueryParam("idNotificaciones")  Long idNotificaciones) throws SQLException, NamingException{
    	Notificaciones notificaciones = facade.eliminarNotificaciones(idNotificaciones);
    	return Response.ok(notificaciones).build();
    }

    @POST
    @Path("/enviar-notificaciones")
    public Response enviarNotificaciones() throws SQLException, NamingException{
    	NotificacionesRespuesta respuesta = facade.enviarNotificaciones();
    	return Response.ok(respuesta).build();
    }
}
