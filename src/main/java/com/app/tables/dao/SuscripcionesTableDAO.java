package com.app.tables.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import com.app.domain.Suscripciones;
import com.app.domain.Usuarios;
import com.app.utils.tables.dao.GenericDaoImpl;

import antlr.ParserSharedInputState;

public class SuscripcionesTableDAO extends GenericDaoImpl<Suscripciones> {

	@PersistenceUnit(unitName = "app")
    private EntityManagerFactory entityManagerFactory;
	
    public List<Map<String, Object>> getSuscripciones(Long idUsuario, String descripcion) {
        StringBuilder countQuery = new StringBuilder();
        countQuery.append(" SELECT s.id_usuario, s.id_suscripcion, cat.id_categoria, " );
		countQuery.append(" CASE " );
		countQuery.append(" WHEN (s.activo IS NULL) THEN false " );
		countQuery.append(" WHEN (s.activo IS NOT NULL) THEN s.activo " );
		countQuery.append(" END AS activo, cat.descripcion    " );
		countQuery.append(" FROM concursapy.categoria cat " );
		countQuery.append(" LEFT JOIN ( " );
		countQuery.append(" SELECT id_usuario, id_suscripcion, c.id_categoria, activo, descripcion " );
		countQuery.append(" FROM concursapy.suscripciones s " );
		countQuery.append(" JOIN concursapy.categoria c ON s.id_categoria = c.id_categoria  " );
		countQuery.append(" WHERE id_usuario =  " + idUsuario ); 
		countQuery.append(" ) s ON s.id_categoria=cat.id_categoria " );
		if(descripcion!= null){
			countQuery.append(" WHERE cat.descripcion LIKE  upper('%" + descripcion+ "%')" ); 
		}
		countQuery.append(" ORDER BY s.descripcion " );
        List<Object[]> oResultList = em.createNativeQuery(countQuery.toString()).
                getResultList();
        List<Map<String, Object>> resultlist = new ArrayList<>();
        for (Object[] oResultArray : oResultList) {
            Map<String, Object> oMapResult = new HashMap<>();
            oMapResult.put("idUsuario", oResultArray[0]);
            oMapResult.put("idSuscripcion", oResultArray[1]);
            oMapResult.put("idCategoria", oResultArray[2]);
            oMapResult.put("activo", oResultArray[3]);
            oMapResult.put("descripcion", oResultArray[4]);
            resultlist.add(oMapResult);
        }
        return resultlist;
    }
    
	public Long crearSuscripciones(Long idUsuario, Long idCategoria) throws Exception {
		Usuarios user = new Usuarios();
		user = em.find(Usuarios.class, idUsuario);
		if(user == null){
    		throw new Exception("No existe el usuario");
		} 
		StringBuilder countQuery = new StringBuilder();
        countQuery.append("select id_suscripcion");
        countQuery.append(" from concursapy.suscripciones");
        countQuery.append(" where id_usuario ="+idUsuario+ " and id_categoria="+idCategoria+" and activo is false");
        List<Object> oResultList = em.createNativeQuery(countQuery.toString()).
                getResultList();
        if(oResultList.size()>0){
        	String idSuscripcion = oResultList.get(0).toString();
    		Suscripciones suscripciones = em.find(Suscripciones.class,  Long.parseLong(idSuscripcion));
    		suscripciones.setActivo(true);
    		suscripciones.setFechaModificacion(new Date());
    		suscripciones.setIdUsuario(user);
    		em = entityManagerFactory.createEntityManager();
    		em.getTransaction().begin();
    		em.merge(suscripciones);
    		em.getTransaction().commit();
    		em.close();
    		return suscripciones.getIdSuscripcion();
        }else{

    		Suscripciones suscripciones = new Suscripciones();
    		suscripciones.setIdUsuario(user);
    		suscripciones.setIdCategoria(idCategoria);
    		suscripciones.setActivo(true);
    		suscripciones.setFechaCreacion(new Date());
    		em = entityManagerFactory.createEntityManager();
    		em.getTransaction().begin();
    		em.persist(suscripciones);
    		em.getTransaction().commit();
    		em.close();
    		return suscripciones.getIdSuscripcion();
        }
        
	}
	
	public Suscripciones inactivarSuscripciones(Long idSuscripcion) {
		em = entityManagerFactory.createEntityManager();
		Suscripciones suscripciones = em.find(Suscripciones.class,  idSuscripcion);
		suscripciones.setFechaModificacion(new Date());
		suscripciones.setActivo(false);
    	em.getTransaction().begin();
    	em.getTransaction().commit();
    	em.close();
    	return suscripciones;
	}

    public Map<String, Object> getTotalSuscripciones(String categoria, String descripcion, String anho) {
        StringBuilder countQuery = new StringBuilder();
        countQuery.append(" SELECT count(*) as suscriptos, c.descripcion " );
        countQuery.append(" FROM concursapy.suscripciones s " );
		countQuery.append(" JOIN concursapy.categoria c ON s.id_categoria = c.id_categoria " );
		countQuery.append(" where s.activo is true  " );
		if(categoria!= null && !categoria.equals("")){

			countQuery.append(" AND unaccent(c.descripcion) LIKE unaccent(upper('%"+categoria+"%'))  " );
		}
		if(anho!= null && !anho.equals("")){
			countQuery.append("  AND EXTRACT(YEAR FROM s.fecha_creacion) = "+ anho );
		}
		countQuery.append(" GROUP BY c.descripcion ORDER BY suscriptos desc; " ); 
		Map<String, Object> respuesta = new HashMap<>();
        List<Object[]> oResultList = em.createNativeQuery(countQuery.toString()).
                getResultList();
		List<Object> suscriptos = new ArrayList<>();
		List<Object> categorias = new ArrayList<>();
        for (Object[] oResultArray : oResultList) {
            suscriptos.add(oResultArray[0]);
            categorias.add(oResultArray[1]);
        }

        respuesta.put("suscriptos", suscriptos);
        respuesta.put("categorias", categorias);
        StringBuilder query = new StringBuilder();
        query.append(" SELECT count(*) as total FROM concursapy.suscripciones s " );
        query.append(" JOIN concursapy.categoria c ON s.id_categoria = c.id_categoria " );
        query.append(" where s.activo is true  " );
		if(categoria!= null && !categoria.equals("")){
			query.append(" AND unaccent(c.descripcion) LIKE unaccent(upper('%"+categoria+"%'))  " );
		}

		if(anho!= null && !anho.equals("")){
			query.append("  AND EXTRACT(YEAR FROM s.fecha_creacion) = "+ anho );
		}
        List<Object> resultList = em.createNativeQuery(query.toString()).
                getResultList();

    	String total = resultList.get(0).toString();
    	respuesta.put("cantidad",  total );
        return respuesta;
    }
}
