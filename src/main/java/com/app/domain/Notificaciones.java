package com.app.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "notificaciones", schema = "concursapy")
public class Notificaciones implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@SequenceGenerator(name = "NOTIFICACIONES_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "concursapy.notificaciones_seq")
	@GeneratedValue(generator = "NOTIFICACIONES_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_notificaciones")
	private Long idNotificaciones;
  	
	@Column(name = "id_concurso")
	private Long idConcurso;

	@Column(name = "id_usuario")
	private Long idUsuario;
	
	@Column(name = "estado_notificacion")
	private Boolean estadoNotificacion;

	@Column(name = "fecha_envio")
	private Date fechaEnvio;
	
	@Column(name = "fecha_lectura")
	private Date fechaLectura;

	public Long getIdNotificaciones() {
		return idNotificaciones;
	}

	public void setIdNotificaciones(Long idNotificaciones) {
		this.idNotificaciones = idNotificaciones;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getIdConcurso() {
		return idConcurso;
	}

	public void setIdConcurso(Long idConcurso) {
		this.idConcurso = idConcurso;
	}

	public Boolean getEstadoNotificacion() {
		return estadoNotificacion;
	}

	public void setEstadoNotificacion(Boolean estadoNotificacion) {
		this.estadoNotificacion = estadoNotificacion;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Date getFechaLectura() {
		return fechaLectura;
	}

	public void setFechaLectura(Date fechaLectura) {
		this.fechaLectura = fechaLectura;
	}
	
	
}
