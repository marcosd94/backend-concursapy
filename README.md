ConcursaPy-Back
============

# Jboss AS 7.1.1 Final: <br>
-Descargar el servidor del siguiente link: http://download.jboss.org/jbossas/7.1/jboss-as-7.1.1.Final/jboss-as-7.1.1.Final.zip


# JDBC Postgres 9.3-1103 : <br>
-Descargar el JDBC del siguiente link: https://jdbc.postgresql.org/download/postgresql-9.3-1103.jdbc41.jar


# Agregar module.xml:
1. Descomprimir el servidor
2. En el directorio del servidor ir a: modules/org/postgresql/main (si no existe el fichero, crearlo)
3. Copiar el JDBC en el fichero anterior.
4. Crear el archivo module.xml
5. Copiar los siguiente en el module.xml:

```xml
<?xml version="1.0" encoding="UTF-8"?>
	<module xmlns="urn:jboss:module:1.0" name="org.postgresql">
	 <resources>
	 <resource-root path="postgresql-9.3-1103.jdbc41.jar"/>
	 </resources>
	 <dependencies>
	 <module name="javax.api"/>
	 <module name="javax.transaction.api"/>
	 </dependencies>
	</module>
```


#SERVICIOS
#### USUARIOS

* OBTENER DATOS DE UN USUARIO

```
GET
https://servicios.sfp.gov.py/app/rest/usuarios/obtener
-> Params
QueryParam: idFirebase

RETURN:
{
    "idFirebase": "2",
    "correoElectronico": "prueba",
    "nombres": "prueba",
    "idUsuario": 24,
    "estadoCivil": "S",
    "apellidos": "prueba",
    "sexo": "M",
    "documento": "prueba",
    "fechaCreacion": "2017-06-19T16:03:37.736+0000",
    "fechaModificacion": null,
    "fechaNacimiento": "1994-06-24"
}

```

* CREAR USUARIOS

```
POST
https://servicios.sfp.gov.py/app/rest/usuarios/agregar
-> Param
OBJECT
{
	"idFirebase": "prueba",
	"nombres": "prueba",
	"apellidos": "prueba",
	"documento": "prueba",
	"estadoCivil": "S",
	"correoElectronico": "prueba",
	"sexo": "M",
	"fechaNacimiento": "1994-06-25"
}

RETURN: USUARIO CREADO

```

* MODIFICAR USUARIOS

```
POST
https://servicios.sfp.gov.py/app/rest/usuarios/modificar
-> Param
OBJECT
{
	"idFirebase": "1",
	"correoElectronico": "pruebaModificacion",
	"nombres": "pruebaModificacion",
	"idUsuario": 23,
	"estadoCivil": "S",
	"apellidos": "prueba",
	"sexo": "M",
	"documento": "prueba",
	"fechaCreacion": "2017-06-19T15:05:21.053+0000",
	"fechaModificacion": null,
	"fechaNacimiento": "1994-06-24"
}
RETURN: USUARIO MODIFICADO

```

* ELIMINAR USUARIOS

```
POST
https://servicios.sfp.gov.py/app/rest/usuarios/eliminar
-> Params
queryParam: Long idUsuario
Example: https://servicios.sfp.gov.py/app/rest/usuarios/delete?idUsuario=1

```

#### NOTIFICACIONES
* LISTADO DE NOTIFICACIONES.

```
GET
https://servicios.sfp.gov.py/app/rest/notificaciones/listar-notificaciones
-> Params 
queryParam: String idFirebase
EXAMPLE:
https://servicios.sfp.gov.py/app/rest/notificaciones/list?idFirebase=25

RETURN:
[
    {
        "inicioPostulacion": "2017-06-13T11:30:00.000+0000",
        "fechaEnvio": "2017-06-21T18:04:31.985+0000",
        "vacancia": 1,
        "puesto": " ABOGADO ",
        "estadoNotificacion": false,
        "finPostulacion": "2017-06-19T19:00:00.000+0000",
        "idConcurso": 4716,
        "idNotificaciones": 14,
        "descripcionOee": " MINISTERIO DE AGRICULTURA Y GANADERIA (MAG) "
    },
    {
        "inicioPostulacion": "2017-06-13T11:30:00.000+0000",
        "fechaEnvio": "2017-06-21T18:04:31.985+0000",
        "vacancia": 2,
        "puesto": " TÉCNICO DE CAMPO II - PARAGUARÍ ",
        "estadoNotificacion": false,
        "finPostulacion": "2017-06-19T19:00:00.000+0000",
        "idConcurso": 4710,
        "idNotificaciones": 17,
        "descripcionOee": " MINISTERIO DE AGRICULTURA Y GANADERIA (MAG) "
    }
]

```

* MODIFICAR NOTIFICACIONES

```
POST
https://servicios.sfp.gov.py/app/rest/notificaciones/modificar-notificacion
-> Param
OBJECT
{
        "fechaEnvio": "2017-06-25T04:00:00.000+0000",
        "fechaLectura": null,
        "idSuscripcion": 4,
        "estadoNotificacion": true,
        "idConcurso": 1,
        "idNotificaciones": 1
}
RETURN: NOTIFICACION MODIFICADA

```

* ENVIAR NOTIFICACIONES

```
POST
https://servicios.sfp.gov.py/app/rest/notificaciones/enviar-notificacionES

RETURN: ESTADO Y CANTIDAD DE NOTIFICACIONES ENVIADAS
{
    "estado": "Notificaciones enviadas correctamente",
    "cantidad": 4
}

OR 

{
    "estado": "No se cuenta con notificaciones a enviar",
    "cantidad": 0
}

```

* MARCAR NOTIFICACION COMO LEIDA

```
POST
https://servicios.sfp.gov.py/app/rest/notificaciones/marcar-notificacion-leida
-> Param
queryParam: Long  idNotificaciones
RETURN: NOTIFICACION MODIFICADA

```       


#### SUSCRIPCIONES
* LISTA DE CATEGORIAS CON SUSCRIPCIONES ACTIVAS POR USUARIOS

```
GET
https://servicios.sfp.gov.py/app/rest/suscripciones/listar-suscripciones
-> Param
queryParam: Long  idUsuario
RETURN:
[
   {
        "idCategoria": 2,
        "idUsuario": 18,
        "idSuscripcion": 12,
        "descripcion": "INFORMATICA",
        "activo": true
    },
    {
        "idCategoria": 2,
        "idUsuario": 25,
        "idSuscripcion": 4,
        "descripcion": "INFORMATICA",
        "activo": false
    }
]

```

* AGREGAR SUSCRIPCIONES / SUSCRIBIRSE A CATEGORIAS

```
POST
https://servicios.sfp.gov.py/app/rest/suscripciones//agregar
-> Params
QueryParam: Long idUsuario,
QueryParam: Long idCategoria

RETURN: SUSCRIPCION CREADA

```

* INACTIVAR SUSCRIPCION

```
POST
https://servicios.sfp.gov.py/app/rest/suscripciones/inactivar-suscripcion
-> Params
QueryParam: Long idSuscripcion

```

* INSERTAR REGISTRO TOKEN

```
POST
https://servicios.sfp.gov.py/app/rest/token/insertar-registration-token
-> Params
QueryParam: Long idUsuario,
QueryParam: String token

RETURN: REGISTRO TOKEN CREADO
