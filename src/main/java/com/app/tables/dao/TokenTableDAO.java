package com.app.tables.dao;

import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import com.app.domain.Token;
import com.app.domain.Usuarios;
import com.app.utils.tables.dao.GenericDaoImpl;

public class TokenTableDAO extends GenericDaoImpl<Token> {

	@PersistenceUnit(unitName = "app")
    private EntityManagerFactory entityManagerFactory;
	
	public Token registrarToken(Long idUsuario, String token) throws SQLException {
		Usuarios user = new Usuarios();
		user = em.find(Usuarios.class, idUsuario);
		if(user == null){
    		throw new SQLException("No existe el usuario");
		} 
		Token itoken = new Token();
		itoken.setIdUsuario(user);
		itoken.setRegistrationToken(token);
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		em.persist(itoken);
		em.getTransaction().commit();
		em.close();
		return itoken;
	}
}
