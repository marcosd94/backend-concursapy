package com.app.tables.facade;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.naming.NamingException;

import com.app.domain.Suscripciones;
import com.app.tables.dao.SuscripcionesTableDAO;
import com.app.utils.tables.facade.DatatablesFacadeImpl;

public class SuscripcionesTableFacade extends
        DatatablesFacadeImpl<Suscripciones> {

    @Inject
    SuscripcionesTableDAO suscripcionesServiciosDao;

    @PostConstruct
    public void init() {
        if (this.dao == null) {
            this.dao = suscripcionesServiciosDao;
        }
    }

    public List<Map<String, Object>> getSuscripciones(Long idUsuario, String descripcion) {
        return suscripcionesServiciosDao.getSuscripciones(idUsuario, descripcion);
    }
    
    public Long crearSuscripciones(Long idUsuario, Long idCategoria) throws Exception {
    	return suscripcionesServiciosDao.crearSuscripciones(idUsuario, idCategoria);
        
    }
    public Suscripciones inactivarSuscripciones(Long id) throws SQLException, NamingException {
    	return suscripcionesServiciosDao.inactivarSuscripciones(id);
        
    }

    public Map<String, Object> getTotalSuscripciones(String categoria, String descripcion, String anho) {
        return suscripcionesServiciosDao.getTotalSuscripciones(categoria, descripcion, anho);
    }
    
}
