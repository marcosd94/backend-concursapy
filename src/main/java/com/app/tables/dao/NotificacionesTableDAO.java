package com.app.tables.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import com.app.domain.Notificaciones;
import com.app.utils.tables.dao.GenericDaoImpl;

public class NotificacionesTableDAO extends GenericDaoImpl<Notificaciones> {

	@PersistenceUnit(unitName = "app")
    private EntityManagerFactory entityManagerFactory;
	
    public List<Map<String, Object>> getNotificaciones(String idFirebase) {
        StringBuilder query = new StringBuilder();
        
		query.append(" select id_notificaciones, n.id_concurso, nivel, descripcion_nivel, entidad, descripcion_entidad, "); 
		query.append(" oee, descripcion_oee, tipos_concurso, objeto_gasto, concepto, ");
		query.append(" clasificacion_ocupacional, categoria, cargo, puesto, vacancia, ");
		query.append(" tipo_funcionario, localidad, domicilio, salario, beneficios_adicionales, ");
		query.append(" estado, fuente_financiamiento, inicio_publicacion, fin_publicacion, ");
		query.append(" id_oficina, inicio_postulacion, fin_postulacion ");
		query.append(" from concursapy.notificaciones n ");
		query.append(" JOIN concursapy.usuarios u ON u.id_usuario = n.id_usuario ");
		query.append(" JOIN concursapy.concursos c ON n.id_concurso = c.id_concurso ");
		query.append(" where u.id_firebase = '"+idFirebase+"'");
		query.append(" order by fecha_envio ");

        List<Object[]> oResultList = em.createNativeQuery(query.toString()).
                getResultList();
        List<Map<String, Object>> resultlist = new ArrayList<>();
        for (Object[] oResultArray : oResultList) {
            Map<String, Object> oMapResult = new HashMap<>();
            oMapResult.put("idNotificaciones", oResultArray[0]);
            oMapResult.put("identificadorConcursoPuesto", oResultArray[1]);
			oMapResult.put("nivel", oResultArray[2]);
			oMapResult.put("descripcionNivel", oResultArray[3]);
			oMapResult.put("entidad", oResultArray[4]);
			oMapResult.put("descripcionEntidad", oResultArray[5]);
			oMapResult.put("oee", oResultArray[6]);
			oMapResult.put("descripcionOee", oResultArray[7]);
			oMapResult.put("tiposConcurso", oResultArray[8]);
			oMapResult.put("objetoGasto", oResultArray[9]);
			oMapResult.put("concepto", oResultArray[10]);
			oMapResult.put("clasificacionOcupacional", oResultArray[11]);
			oMapResult.put("categoria", oResultArray[12]);
			oMapResult.put("cargo", oResultArray[13]);
			oMapResult.put("puesto", oResultArray[14]);
			oMapResult.put("vacancia", oResultArray[15]);
			oMapResult.put("tipoFuncionario", oResultArray[16]);
			oMapResult.put("localidad", oResultArray[17]);
			oMapResult.put("domicilio", oResultArray[18]);
			oMapResult.put("salario", oResultArray[19]);
			oMapResult.put("beneficiosAdicionales", oResultArray[20]);
			oMapResult.put("estado", oResultArray[21]);
			oMapResult.put("fuenteFinanciamiento", oResultArray[22]);
			oMapResult.put("inicioPublicacion", oResultArray[23]);
			oMapResult.put("finPublicacion", oResultArray[24]);
			oMapResult.put("idOficina", oResultArray[25]);
			oMapResult.put("inicioPostulacion", oResultArray[26]);
			oMapResult.put("finPostulacion", oResultArray[27]);
            resultlist.add(oMapResult);
        }
        return resultlist;
    }
    
	public String crearNotificaciones(Notificaciones noti) throws SQLException, NamingException {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		em.persist(noti);
		em.getTransaction().commit();
		em.close();
		return "OK";
	}
	
	public Notificaciones eliminarNotificaciones(Long id) {

		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		Notificaciones notificaciones= em.find(Notificaciones.class, id);
		em.remove(notificaciones);
    	em.getTransaction().commit();
    	em.close();
    	return notificaciones;
	}

	
	public String modificarNotificacion(Notificaciones notificaciones) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		em.find(Notificaciones.class, notificaciones.getIdNotificaciones());
		em.merge(notificaciones);
		em.getTransaction().commit();
		em.close();
		return "OK";
	}

	
	public Notificaciones marcarNotificacionLeida(Long idNotificacion) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		Notificaciones notificaciones= em.find(Notificaciones.class, idNotificacion);
		notificaciones.setFechaLectura(new Date());
		notificaciones.setEstadoNotificacion(true);
		em.merge(notificaciones);
		em.getTransaction().commit();
		em.close();
		return notificaciones;
	}

    public List<Map<String, Object>> getListaNotificaciones() {
        StringBuilder countQuery = new StringBuilder();     
        
        countQuery.append(" SELECT DISTINCT t.registration_token as token  ");
        countQuery.append(" FROM concursapy.notificaciones_usuarios nu ");
        countQuery.append(" JOIN concursapy.token t ON t.id_usuario = nu.id_usuario  ");
        countQuery.append(" WHERE estado_notificacion is false ");

        List<String> oResultList = em.createNativeQuery(countQuery.toString()).
                getResultList();

        List<Map<String, Object>> resultlist = new ArrayList<>();
        for (String oResultArray : oResultList) {
            Map<String, Object> oMapResult = new HashMap<>();
            oMapResult.put("token", oResultArray);
            resultlist.add(oMapResult);
        }
        return resultlist;
    }
    

    public void marcarNotificacionesEnviadas() {
	    try{
	    	em.getTransaction().begin();
	    	Query q = em.createNativeQuery(" UPDATE concursapy.notificaciones_usuarios SET estado_notificacion = true ");
		    q.executeUpdate();
	    	em.getTransaction().commit();
	    }catch (PersistenceException pe){
	        pe.printStackTrace();
	    }
    }
    
}
