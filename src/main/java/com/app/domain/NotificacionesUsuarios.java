package com.app.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "notificaciones_usuarios", schema = "concursapy")
public class NotificacionesUsuarios implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@SequenceGenerator(name = "NOTIFICACIONES_USUARIOS_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "concursapy.notificaciones_usuarios_seq")
	@GeneratedValue(generator = "NOTIFICACIONES_USUARIOS_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_notificaciones_usuarios")
	private Long idNotificacionesUsuarios;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario")
	@NotNull
	private Usuarios idUsuario;
 	
	@Column(name = "fecha_envio")
	private Date fechaEnvio;
	
	@Column(name = "estado_notificacion")
	private Boolean estadoNotificacion;

	public Long getIdNotificacionesUsuarios() {
		return idNotificacionesUsuarios;
	}

	public void setIdNotificacionesUsuarios(Long idNotificacionesUsuarios) {
		this.idNotificacionesUsuarios = idNotificacionesUsuarios;
	}

	public Usuarios getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Usuarios idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Boolean getEstadoNotificacion() {
		return estadoNotificacion;
	}

	public void setEstadoNotificacion(Boolean estadoNotificacion) {
		this.estadoNotificacion = estadoNotificacion;
	}

}
